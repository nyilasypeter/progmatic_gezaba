/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba.children;


import com.progmatic.gezaba.Child;
import com.progmatic.gezaba.Direction;
import java.util.UUID;

/**
 *
 * @author peti
 */
public class DullChild implements Child{
    
    protected String namePostFix =  UUID.randomUUID().toString();

    @Override
    public String getName() {
        return this.getClass().getName() + " " + namePostFix;
    }


    @Override
    public Direction step() {
        return Direction.FORWARD;
    }

    @Override
    public void declareWinner(Child c) {
    }
    
}
