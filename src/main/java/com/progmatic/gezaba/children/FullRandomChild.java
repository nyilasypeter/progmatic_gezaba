/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba.children;

import com.progmatic.gezaba.Direction;
import java.util.Random;

/**
 *
 * @author peti
 */
public class FullRandomChild extends DullChild{

    protected Random r = new Random();

    protected Direction stepRandomly() {
        int nextInt = r.nextInt(3);
        if (nextInt == 0) {
            return Direction.BACKWARD;
        } else if (nextInt == 1) {
            return Direction.STAY;
        } else if (nextInt == 2) {
            return Direction.FORWARD;
        } else {
            throw new RuntimeException("Programming error, nextInt must be 0, 1, or 2!");
        }
    }

    @Override
    public Direction step() {
        return stepRandomly();
    }
    
    
}
