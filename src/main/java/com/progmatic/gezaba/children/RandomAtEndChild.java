/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba.children;

import com.progmatic.gezaba.Child;
import com.progmatic.gezaba.Direction;
import com.progmatic.gezaba.Gym;
import java.util.UUID;

/**
 *
 * @author peti
 */
public class RandomAtEndChild extends FullRandomChild {

    
    private int stepNr = -1;    
    private final int startWalkRandomlyAt;

    public RandomAtEndChild(int startWalkRandomlyAt) {
        this.startWalkRandomlyAt = startWalkRandomlyAt;
    }
    
    

    @Override
    public String getName() {
        return this.getClass().getName() + " who starts walking randomly with "  + startWalkRandomlyAt + " positions before the end " + namePostFix;
    }
    
    

    @Override
    public Direction step() {
        stepNr++;
        if (stepNr >= Gym.gymLength - startWalkRandomlyAt) {
            return stepRandomly();

        } else {
            return Direction.FORWARD;
        }
    }

    @Override
    public void declareWinner(Child c) {
        stepNr = -1;
    }


}
