/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author peti
 */
public class Gym {

    List<ChildWithPosition> tracks = new ArrayList<>();
    public static int gymLength = 10;
    private Child winner = null;

    public Gym() {
    }
    
    public Gym(List<Child> children) {
        for (Child child : children) {
            addChild(child);
        }
    }
    
   
    
    public void clap() {
        for (ChildWithPosition cp : tracks) {
            if (!cp.hasChildReachedWall()) {
                Direction step = cp.getChild().step();
                if (step == Direction.BACKWARD) {
                    if (cp.getPosition() > 0) {
                        cp.stepBack();
                    }
                }
                else if(step == Direction.FORWARD){
                    if (cp.getPosition() < gymLength) {
                            cp.stepForward();
                        }
                }
            }

        }
        ChildWithPosition winnerCp = calcWinner();
        if(winnerCp != null){
            winner = winnerCp.getChild();
        }
    }

    private ChildWithPosition calcWinner() {
        List<ChildWithPosition> winners = new ArrayList<>();
        for (ChildWithPosition cp : tracks) {
            if (cp.getPosition() == gymLength) {
                winners.add(cp);
            }
        }
        if (winners.size() == 1) {
            return winners.get(0);
        } else {
            for (ChildWithPosition cp : winners) {
                cp.childReachedWall();
            }
            return null;
        }
    }

    public Child getWinner() {
        return winner;
    }

    public boolean hasWinner() {
        return winner != null;
    }

    private void delcareWinner() {
        if(winner==null){
            Logger.log("no winner");
        }
        else{
            Logger.log("the winner is: " +  winner.getClass().getName());
        }
        for (ChildWithPosition child : tracks) {
            child.getChild().declareWinner(winner);
        }
    }

    public void restart() {
        delcareWinner();
        for (ChildWithPosition cp : tracks) {
            cp.reset();
        }
        winner = null;
    }
    
    public void addChild(Child ch){
        ChildWithPosition cw = new ChildWithPosition();
        cw.setChild(ch);
        tracks.add(cw);
    }
}
