/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba.config;

import com.progmatic.gezaba.GezaBa;
import com.progmatic.gezaba.Gym;
import com.progmatic.gezaba.children.DullChild;
import com.progmatic.gezaba.children.FullRandomChild;
import com.progmatic.gezaba.children.RandomAtEndChild;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author peti
 */
@Configuration
public class SpringConfig {

    @Bean
    public GezaBa gezaBa() {
        return new GezaBa(gym());
    }

    @Bean
    public Gym gym() {
        Gym gym = new Gym();
        DullChild dc = new DullChild();
        DullChild dc2 = new DullChild();
        FullRandomChild rc = new FullRandomChild();
        FullRandomChild rc2 = new FullRandomChild();
        RandomAtEndChild rce1 = new RandomAtEndChild(1);
        RandomAtEndChild rce2 = new RandomAtEndChild(2);
        RandomAtEndChild rce3 = new RandomAtEndChild(3);
        RandomAtEndChild rce4 = new RandomAtEndChild(4);
        RandomAtEndChild rce5 = new RandomAtEndChild(5);
        RandomAtEndChild rce6 = new RandomAtEndChild(6);
        gym.addChild(dc);
        gym.addChild(dc2);
        gym.addChild(rc);
        gym.addChild(rc2);
        gym.addChild(rce1);
        gym.addChild(rce2);
        gym.addChild(rce3);
        gym.addChild(rce4);
        gym.addChild(rce5);
        gym.addChild(rce6);
        return gym;
    }
    
    @Bean
    public GezaBa gezaBaSimple() {
        return new GezaBa(gymSimple());
    }

    @Bean
    public Gym gymSimple() {
        Gym gym = new Gym();
        DullChild dc = new DullChild();
        DullChild dc2 = new DullChild();
        FullRandomChild rc = new FullRandomChild();
        RandomAtEndChild rce1 = new RandomAtEndChild(1);
        gym.addChild(dc);
        gym.addChild(dc2);
        gym.addChild(rc);
        gym.addChild(rce1);
        return gym;
    }
}
