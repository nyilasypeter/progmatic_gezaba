/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba;

/**
 *
 * @author peti
 */
public class ChildWithPosition {
    private Child child;
    private int position = 0;

    public ChildWithPosition() {
    }

    public ChildWithPosition(Child child) {
        this.child = child;
    }
    

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public int getPosition() {
        return position;
    }

    public void stepForward() {
        this.position++;
    }
    
    public void stepBack() {
        this.position++;
    }
    
    public void childReachedWall(){
        this.position = Integer.MAX_VALUE;
    }
    
    public boolean hasChildReachedWall(){
        return this.position == Integer.MAX_VALUE;
    }
    public void reset(){
        this.position = 0;
    }
}
