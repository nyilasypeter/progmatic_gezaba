/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.gezaba;


import com.progmatic.gezaba.config.SpringConfig;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author peti
 */
public class GezaBa {
    
    public static int simulationSize = 100;
    private Gym gym;
    private  Map<String, Integer> noteBookOfWinners = new HashMap<>();

    public GezaBa(Gym gym) {
        this.gym = gym;
    }
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logger.verbose = false;
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        GezaBa gb = context.getBean("gezaBaSimple", GezaBa.class);
        gb.simulate();
        System.out.println();
        System.out.println("********************One simulation is over********************");
        System.out.println();
        gb = context.getBean("gezaBa", GezaBa.class);
        gb.simulate();
    }

    public void simulate() {
        for (int simIdx = 0; simIdx < simulationSize; simIdx++) {
            gym.restart();
            for (int i = 0; i < 100; i++) {
                gym.clap();
                if (gym.hasWinner()) {
                    takeNotes(gym.getWinner());
                    break;
                }
                if(i==99){
                    Logger.log("there was no winner this time.");
                }
            }
        }
        printNoteBook();

    }

    private void takeNotes(Child winner) {
        String winnerName = winner.getName();
        Integer nrOfWins = noteBookOfWinners.putIfAbsent(winnerName, 0);
        if(nrOfWins == null){
            nrOfWins = 0;
        }
        noteBookOfWinners.put(winnerName, nrOfWins+1);
    }

    private void printNoteBook() {
        Set<Map.Entry<String, Integer>> entrySet = noteBookOfWinners.entrySet();
        for (Map.Entry<String, Integer> entry : entrySet) {
            System.out.println(String.format("Chid: '%s' won %d times", entry.getKey(), entry.getValue()));
        }
    }

    public Map<String, Integer> getNoteBookOfWinners() {
        return noteBookOfWinners;
    }

}
